# Weezo PKG Packager

Pacote para Laravel 5 desenvolvido por **Weezo Soluções em Informática**

> **EM DESENVOLVIMENTO**

> Pacote laravel para criação de novos pacotes, desenvolvido com base no pacote [Laravel Packager](https://github.com/Jeroen-G/laravel-packager).

## Índice

* [Instalação](#instalação)
    * [Dependências](#1-dependência)
    * [Provider](#2-provider)
* [Modo de usar](#modo-de-usar)
* [Idiomas](#idiomas)
* [Desenvolvedores](#desenvolvedores)
* [Créditos](#créditos)

## Instalação

### 1. Dependência
Adicione o repositório e o require no `composer.json`:
```json
{
    "repositories":[
        {
            "type": "vcs",
            "url": "https://github.com/weezo/pkg-packager"
        }
    ],
    "require": {
        "weezo/packager": "dev-development"
    }
}
```

### 2. Provider
Adicione o provider no arquivo `config/app.php` , atenção para a vírgula (,) no final:
```php
'providers' => [
    Weezo\Packager\Providers\PackagerServiceProvider::class,
]
```

## Modo de usar

### Criando novo pacote
O comando abaixo irá lidar com praticamente tudo para você. Ele vai criar um diretório de pacotes (Packages), cria dentro dele o diretório vendor/pacote, adiciona uma estrutura de pastas para o pacote, configura composer.json, cria um Service Provider, registra o pacote em config/app.php e no composer.json do laravel.
Então você pode começar imediatamente com apenas este comando:
```shell
$ artisan packager:new Vendor Pacote
```

Para definir uma descrição para o pacote, adicione a opção:

```shell
$ artisan packager:new Vendor Pacote -D "Descrição do pacote"
```

Depois execute o dump do autoload do composer:
```shell
$ composer dump-autoload
```

## Idiomas
* Português (Brasil)

## Desenvolvedores
* [Juliana Macêdo](https://github.com/jullymac)

## Créditos
* [Laravel Packager (Desenvolvido por JeroenG)](https://github.com/Jeroen-G/laravel-packager)
* [League Packages Skeleton (Desenvolvido por ThePhpLeague)](https://github.com/thephpleague/skeleton)
