<?php

namespace Weezo\Packager\Commands;

use Illuminate\Console\Command;
use Weezo\Packager\Helpers\PackagerHelper;

/**
 * Create a brand new package.
 *
 * @package Packager
 * @author JeroenG
 *
 **/
class NewCmd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'packager:new
                            {vendor : Nome do vendor em camelCase}
                            {package : Nome do pacote em camelCase}
                            {--D|description=Descrição do pacote : Descrição do pacote}
                            {--P|path=packages : Nome do diretório onde ficará o pacote}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cria um novo pacote.';

    /**
     * Packager helper class.
     * @var object
     */
    protected $helper;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(PackagerHelper $helper)
    {
        parent::__construct();
        $this->helper = $helper;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        // Start the progress bar
        $bar = $this->helper->barSetup($this->output->createProgressBar(9));
        $bar->start();

        // Common variables
        $vendorName = $this->argument('vendor');
        $vendor_name = strtolower(preg_replace('/([a-zA-Z])(?=[A-Z])/', '$1-', $vendorName));

        $packageName = $this->argument('package');
        $package_name = strtolower(preg_replace('/([a-zA-Z])(?=[A-Z])/', '$1-', $packageName));
        $package_description = $this->option('description');

        $dir = $this->option('path');
        $path = getcwd().'/'.$dir.'/';
        $fullPath = $path.$vendor_name.'/'.$package_name;
        $skeletonPath = __DIR__.'/../../Resources/skeletons';

        $tagConfigs = $this->helper->getConfigTags();
        foreach ($tagConfigs['vals'] as $k => $v){ $tagConfigs['vals'][$k] = eval('return '.$v.';'); }

        // Começa a criar o pacote..
        $this->info('Criando pacote '.$vendor_name.'\\'.$package_name.'...');
        $this->helper->checkExistingPackage($path, $vendor_name, $package_name);
        $bar->advance();

        // Cria o diretório de pacotes
        $this->info('Criando o diretório de pacotes...');
        $this->helper->makeDir($path);
        $bar->advance();

        // Cria o diretório Vendor
        $this->info('Criando diretório do vendor...');
        $this->helper->makeDir($path.$vendor_name);
        $bar->advance();

        // Pega o esqueleto e renomeia algumas pastas
        $this->info('Copiando skeleton...');
        $this->helper->copy($skeletonPath.'/package',$fullPath);
        rename($fullPath.'/src/PackageName', $fullPath.'/src/'.$packageName);
        rename($fullPath.'/tests/PackageName', $fullPath.'/tests/'.$packageName);
        $bar->advance();

        // Cria o Service Provider
        $this->info('Criando o service provider...');
        $newProvider = $fullPath.'/src/'.$packageName.'/Providers/'.$packageName.'ServiceProvider.php';
        $this->helper->replaceAndSave($skeletonPath.'/files/ServiceProvider.php.sk', $tagConfigs['tags'], $tagConfigs['vals'], $newProvider);
        $bar->advance();

        // Cria o composer.json
        $this->info('Criando o composer.json...');
        $newComposer = $fullPath.'/composer.json';
        $this->helper->replaceAndSave($skeletonPath.'/files/composer.json.sk', $tagConfigs['tags'], $tagConfigs['vals'], $newComposer);
        $bar->advance();

        // Cria o README.md
        $this->info('Criando o README.md...');
        $newReadMe = $fullPath.'/README.md';
        $this->helper->replaceAndSave($skeletonPath.'/files/README.md.sk', $tagConfigs['tags'], $tagConfigs['vals'], $newReadMe);
        $bar->advance();

        // Adicionar o namespace no composer do Laravel
        $this->info('Adicionando pacote no composer.json do laravel...');
        $requirement_search = '"psr-4": {';
        $requirement_replace = $requirement_search.'
            "'.$vendorName.'\\\\'.$packageName.'\\\\": "'.$dir.'/'.$vendor_name.'/'.$package_name.'/src/'.$packageName.'/",';
        $this->helper->replaceAndSave(getcwd().'/composer.json', $requirement_search, $requirement_replace);
        $bar->advance();

        //Adiciona service provider no config/app.php
        $this->info('Adicionando service provider no config/app.php...');
        $appConfigLine_search = 'Weezo\Packager\Providers\PackagerServiceProvider::class,';
        $appConfigLine_replace = $appConfigLine_search.'

        '.$vendorName.'\\'.$packageName.'\\Providers\\'.$packageName.'ServiceProvider::class,';
        $this->helper->replaceAndSave(getcwd().'/config/app.php', $appConfigLine_search, $appConfigLine_replace);
        $bar->advance();

        // Finished creating the package, end of the progress bar
        $bar->finish();
        $this->info('Pacote criado com sucesso!');
        $this->output->newLine(2);
        $bar = null;


    }
}
